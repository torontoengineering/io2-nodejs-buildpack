# IO2 - Cloud Foundry Node.js Buildpack

### Customizing this Buildpack

Before modifying a buildpack that is in production, make a development branch of the repo.  You can use a specific version of the build pack in PCF if you use a branch/tag identifier #v1.5.20.

#### For use with Ubuntu deb packages.

Using fresh VM running Ubunu 14.04.5 LTS (PCF stemcell uses Ubuntu 14 Trusty) download the package(s) required for the apps environment.
Use -f to force all dependacies to be intstalled .
Copy files from the apt cache to a local folder.

	sudo apt-get clean
	sudo apt-get --download-only install ghostscript -f -y
	ls -l /var/cache/apt/archives
	mkdir ~/ghostscript
	cp /var/cache/apt/archives/*.deb ~/ghostscript

NOTE: I use Oracle VBox to run a fresh image of Ubuntu Trusty.

Upload this folder into AWS S3 and make it public.
The location of the files should be in a sub folder of S3://json-public-main/packages.
For example, ckm deb packages are in S3://json-public-main/packages/ckm/deb.  All of the package folders are under this.

	S3://json-public-main/packages/ckm/deb/chromedriver/
	S3://json-public-main/packages/ckm/deb/graphicsmagick/
	S3://json-public-main/packages/ckm/deb/python/
	S3://json-public-main/packages/ckm/deb/software-properties-common/

Create a shell script to download the deb packages from S3 and a folder called 'deb'.

DOWNLOAD_URL="https://s3.amazonaws.com/json-public-main/packages/ckm/deb/graphicsmagick/"
  
	if [ ! -d deb ];
	then
		mkdir deb
	fi
   
	cd deb
	wget --quiet $DOWNLOAD_URL"ghostscript_9.10~dfsg-0ubuntu10.10_amd64.deb"
	wget --quiet $DOWNLOAD_URL"graphicsmagick_1.3.28-rwky1~trusty_amd64.deb"
	:
	:
	wget --quiet $DOWNLOAD_URL"libtiff5_4.0.3-7ubuntu0.9_amd64.deb"
	cd -

Upload the script to S3.

Edit the io2-nodejs-buildpack/bin/compile file to download the script, and run it.

	echo "-----> Install graphicsmagick"
	   
	cd $BUILD_DIR
	cd $VENDOR_DIR
	   
	DOWNLOAD_URL="https://s3.amazonaws.com/json-public-main/packages/ckm/get-graphicsmagick-packages.sh"
	wget --quiet $DOWNLOAD_URL
	chmod 755 get-graphicsmagick-packages.sh
	./get-graphicsmagick-packages.sh

The compile script is set up to run extract all file in the 'deb' folder.

If your packages require special environment variables, add them to the profile script section.

	echo  "Writing profile script"
	PROFILE_PATH="$BUILD_DIR/.profile.d/000_myapt.sh"
	cat <<'EOF' >$PROFILE_PATH
	echo hello
	export PATH="$PATH:$HOME/.apt/usr/lib/chromium-browser:$HOME/.apt/usr/bin"
	:
	:
	:
	export MAGICK_CONFIGURE_PATH="$HOME/.apt/usr/lib/GraphicsMagick-1.3.28/config"
	EOF
	chmod 755 $PROFILE_PATH

NOTE: This script run during the buildpack compilation as part of the CF push.

#### For use with ZIP/TAR packages.

Aquire the zip package and upload to S3.  Place the zip in a folder that identifies it's usage.

	s3://json-public-main/packages/ckm/ffmpeg-release-64bit-static.tar.xz

Edit the io2-nodejs-buildpack/bin/compile file to download the script, and extract it.  Add any extra environment variables to the /.profile.d script.  This script run during the buildpack compilation as part of the CF push.

	# installing ffmpeg

	BUILD_DIR=$1
	VENDOR_DIR="vendor"
	
	echo "-----> Install ffmpeg"
	DOWNLOAD_URL="https://s3.amazonaws.com/json-public-main/packages/ckm/ffmpeg-release-64bit-static.tar.xz"
	
	echo "DOWNLOAD_URL = " $DOWNLOAD_URL
	
	cd $BUILD_DIR
	mkdir -p $VENDOR_DIR
	cd $VENDOR_DIR
	wget --quiet $DOWNLOAD_URL
	tar xf ffmpeg-release-64bit-static.tar.xz
	rm ffmpeg-release-64bit-static.tar.xz
	
	echo "exporting PATH and LIBRARY_PATH"
	PROFILE_PATH="$BUILD_DIR/.profile.d/1_ffmpeg.sh"
	mkdir -p $(dirname $PROFILE_PATH)
	echo 'export PATH="$HOME/vendor/$(basename $(ls -d /home/vcap/app/vendor/ffmpeg*)):$PATH"' >> $PROFILE_PATH
	chmod 755 $PROFILE_PATH

#### Deploy the app with the build pack.

Modify the apps manifest.yml to use the buildpack.  Use the tag/branch identifier to use a specific version of the buildpack

	---
	applications:
	- name: ckm-content
	  buildpack: https://bitbucket.org/torontoengineering/io2-nodejs-buildpack#v1
	  memory: 3G
	  disk_quota: 8G
	  command: NODE_ENV=production node --inspect server/app.js
	  health-check-type: none
	  env:
	    NODE_ENV: production

#### Tagging

Once the buildpack is acceptable for use in production,  tag if with a version number.
This will ensure that your apps deployment will allways use the specific version of the buildpack. 
Otherwise it will always use the latest in master.
Because this repo is forked from the cloudfoundry repo, make your tag specific to its use such as 'ckm-content-V1.0.0'.

#### Testing/Troubleshooting the buildpack.

Once the app is deployed you can ssh into the app container.
Once you are in, issue the commands to setup the environment as the app, and examine the environment and it's paths.
You can also issue version checks of supporting apps.

	C:\Users\jepp>cf ssh ckm-content
	vcap@d554f94c-c2d6-4aee-60aa-fd8cc0467397:~$ export HOME=/home/vcap/app && export TMPDIR=/home/vcap/tmp && cd /home/vcap/app && [ -d /home/vcap/app/.profile.d ] && for f in /home/vcap/app/.profile.d/*.sh; do source "$f"; done	
	vcap@d554f94c-c2d6-4aee-60aa-fd8cc0467397:~$ env
	CF_INSTANCE_ADDR=10.0.16.65:64018
	TERM=xterm
	:
	:
	PATH=/home/vcap/app/vendor/ghostscript-9.23-linux-x86_64:/home/vcap/app/vendor/ffmpeg-3.4.2-64bit-static:/home/vcap/app/.cloudfoundry/0/bin:/bin:/usr/bin:/home/vcap/app/.apt/usr/lib/chromium-browser:/home/vcap/app/.apt/usr/bin:/home/vcap/app/.apt/usr/lib/GraphicsMagick-1.3.28:/home/vcap/app/.apt/usr/lib/GraphicsMagick-1.3.28/config:/home/vcap/app/bin:/home/vcap/app/node_modules/.bin
	:
	:
	vcap@d554f94c-c2d6-4aee-60aa-fd8cc0467397:~$ ffmpeg -version
	ffmpeg version 3.4.2-static https://johnvansickle.com/ffmpeg/  Copyright (c) 2000-2018 the FFmpeg developers
	:
	:
	vcap@d554f94c-c2d6-4aee-60aa-fd8cc0467397:~$


### Acknowledgements

Initially forked from the Cloudfoundry [nodejs buildpack](https://github.com/cloudfoundry/nodejs-buildpack).

